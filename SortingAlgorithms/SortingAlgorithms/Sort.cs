﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    static class Sort
    {
        private static void Swap(ref int a, ref int b)
        {
            int temp = a;
            a = b;
            b = temp;
        }
        public static void BubbleSort(ref int[] arr)
        {
            int n = arr.Length;
            for (int i = 0; i < n; i++)
            {
                bool isSwaped = false;
                for (int j = 0; j < n - 1 - i; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        Swap(ref arr[j], ref arr[j + 1]);
                        isSwaped = true;
                    }
                }
                if (!isSwaped)
                {
                    break;
                }
            }
        }

        public static void SelectionSort(ref int[] arr)
        {
            int n = arr.Length;
            for (int i = 0; i < n; i++)
            {
                int minInd = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (arr[j] < arr[minInd])
                    {
                        minInd = j;
                    }
                }
                Swap(ref arr[i], ref arr[minInd]);

            }
        }
        public static void InsertionSort(ref int[] arr)
        {
            int n = arr.Length;
            for (int i = 1; i < n; i++)
            {
                int temp = arr[i];
                int insertInd = i;
                while (insertInd > 0 && arr[insertInd - 1] > temp)
                {
                    arr[insertInd] = arr[insertInd - 1];
                    insertInd--;
                }
                arr[insertInd] = temp;
            }
        }


        public static void MergeSort(ref int[] arr)
        {
            int n = arr.Length;
            throw new NotImplementedException();
        }

        public static void QuickSort(ref int[] arr)
        {
            int n = arr.Length;
            throw new NotImplementedException();
        }

        public static void HeapSort(ref int[] arr)
        {
            int n = arr.Length;
            throw new NotImplementedException();
        }
    }
}
