﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 4, 6, 1, 9, 3 };
            int[] arr1 = new int[] { 1, 3, 4, 6, 9 };
            int[] arr2 = new int[] { 9, 6, 4, 3, 1 };
            PrintArray(arr);
            Sort.SelectionSort(ref arr);
            PrintArray(arr);
            Console.ReadKey();
        }
        static void PrintArray(int[] x)
        {
            int n = x.Length;
            if (n <= 0)
            {
                return;
            }
            Console.Write("X = { ");
            for (int i = 0; i < n - 1; i++)
            {
                Console.Write($"{x[i]}, ");
            }
            Console.Write($"{x[n - 1]} }}");
            Console.WriteLine();
        }
    }
}
